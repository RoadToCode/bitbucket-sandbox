import React, { Component } from 'react';
import logo from '../logo.svg';

import '../styles/App.css';
import styled from 'styled-components';
import { Grid, Cell } from "styled-css-grid";

// Components
import Header from './Header';
import TraditionalGrid from './TraditionalGrid';


 
const MyGrid = () => (
  <Grid columns={2} gap="2px">
    <Cell>foo</Cell>
    <Cell height={2}>bar</Cell>
    <Cell width={2}>baz</Cell>
  </Grid>
);

const Title = styled.h1`
  color: blue;
  font-size: 100px;
`

class App extends Component {
  render() {
    return (
      <div>
        <TraditionalGrid />
        <Title> Welcome Sheebz! </Title>
        <Header />
      </div>
    );
  }
}

export default App;
