import React from "react";
import { stripIndent } from "common-tags";
import { flatMap, range } from "lodash-es";
import Grid from "./DemoGrid";
import Cell from "./DemoCell";
import Example from "./Example";
import Heading from "./Heading";

const rows = counts =>
  flatMap(counts, number =>
    range(number).map(i =>
      <Cell width={12 / number} key={`${number}_${i}`}>
        {i + 1}/{number}
      </Cell>
    )
  );

const TraditionalGrid = () =>
  <article>
    {/* <Grid columns={12} minRowHeight="45px">
      {rows([12, 6, 4, 2, 1])}
      {rows([2])}
    </Grid> */}

    <Grid
  columns={"100px 1fr 100px"}
  rows={"minmax(45px,auto) 1fr minmax(45px,auto)"}>
  <Cell width={3}>
    <h1>Header</h1>
  </Cell>

  <Cell>Menu</Cell>
  <Cell>Content</Cell>
  <Cell>Ads</Cell>

  <Cell width={3}>
    Footer
  </Cell>
</Grid>
  </article>;

// const code = stripIndent`
//   <Grid columns={12}>
//     <Cell width={1}>1/12</Cell>
//     <Cell width={1}>2/12</Cell>
//     ...
//     <Cell width={2}>1/6</Cell>
//     <Cell width={2}>2/6</Cell>
//     ...
//   </Grid>
// `;

const TraditionalGridSection = () =>
  <section>
    
    <Example
      // input={
      //   <pre>
      //     <code>
      //       {code}
      //     </code>
      //   </pre>
      // }
      output={<TraditionalGrid />}
      path={"website/components/sections/TraditionalGrid.js"}
    />
  </section>;

export default TraditionalGridSection;